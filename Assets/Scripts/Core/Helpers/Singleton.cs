using UnityEngine;

namespace Core.Helpers
{
    public class SingletonMonoBehaviour<T> : DontDestroyMonoBehaviour where T : DontDestroyMonoBehaviour
    {
        private static T _instance;
        
        public static T Instance => GetInstance();

        protected void Awake()
        {
            if (_instance != null) Destroy(this);
            else GetInstance();
        }

        protected static T GetInstance()
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                    
                if (_instance == null)
                {
                    _instance = new GameObject().AddComponent<T>();
                }
                    
                if (_instance.dontDestroyOnLoad) DontDestroyOnLoad(_instance);
            }

            return _instance;
        }
    }
    
    public class DontDestroyMonoBehaviour : MonoBehaviour
    {
        public bool dontDestroyOnLoad;
    }
}