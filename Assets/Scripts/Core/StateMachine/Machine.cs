using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.StateMachine
{
    public class Machine<T> where T : StateMachine<T>
    {
        public T CurrentState { get; private set; }

        public Machine(string name)
        {
            CurrentState = null;
        }
        
        public void Start(T state)
        {
            ChangeToState(state);
        }

        public void ChangeToState(T state)
        {
            CurrentState?.Exit();
            Debug.Log(state.Name);
            CurrentState = state;
            CurrentState.Enter();
        }
    }
}