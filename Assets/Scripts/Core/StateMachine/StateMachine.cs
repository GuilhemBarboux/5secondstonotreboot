using System.Collections.Generic;
using UnityEngine;

namespace Core.StateMachine
{
    public class StateMachine<T> where T : StateMachine<T>
    {
        private readonly Machine<T> _owner;

        public string Name { get; }

        public StateMachine(string name, Machine<T> owner)
        {
            Name = name;
            _owner = owner;
        }
        
        public void ChangeToState(T state)
        {
            _owner.ChangeToState(state);
        }
        public virtual void Enter() {}
        public virtual void Exit() {}
    }
}