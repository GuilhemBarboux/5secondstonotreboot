using UnityEngine;
using UnityEngine.InputSystem;

namespace Core.Player
{
    public struct PlayerControllerInputs
    {
        public Vector2 direction;
        public bool jump;
        public bool pause;
        public bool crunch;
    }
    
    public class PlayerController : MonoBehaviour
    {
        public PlayerControllerInputs inputs;
        
        public void OnPause(InputValue value)
        {
            inputs.pause = !inputs.pause;
        }
        
        public void OnCrunch(InputValue value)
        {
            inputs.crunch = value.isPressed;
        }
        
        public void OnJump(InputValue value)
        {
            inputs.jump = value.isPressed;
        }
        
        public void OnMove(InputValue value)
        {
            inputs.direction = value.Get<Vector2>();
        }
        
        public virtual void OnMenu(InputValue value) {}
    }
}