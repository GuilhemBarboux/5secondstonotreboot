﻿﻿using UnityEngine;

namespace Core.Movement
{
    public struct MovementInfo
    {
        // Position between movement
        public Vector2 position;
        public Vector2 previousPosition;
        
        // Next movement
        public Vector2 movement;
    }

    public class Movement2D
    {
        public MovementInfo Info => _state;
        public Vector2 Velocity => _state.movement;

        private MovementInfo _state;
        private readonly Rigidbody2D _r;

        public Movement2D(Rigidbody2D r)
        {
            _r = r;
        }

        public void UpdatePosition(Vector2 velocity)
        {
            _state.movement += velocity;
            _state.previousPosition = _r.position;
            _state.position = _state.previousPosition + _state.movement;
            
            _r.MovePosition(_state.position);
            _state.movement = Vector2.zero;
        }
        
        public void Teleport(Vector2 position)
        {
            _state.previousPosition += position - _state.position;
            _state.position = position;
            _r.MovePosition(_state.position);
        }
    }
}
