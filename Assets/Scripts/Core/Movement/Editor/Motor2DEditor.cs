using UnityEditor;
using UnityEngine;

namespace Core.Movement
{
    [CustomEditor(typeof(Motor2D))]
    public class Motor2DEditor : Editor
    {
        private Motor2D _m;

        private void OnEnable()
        {
            if (_m == null) _m = (Motor2D) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.BeginVertical("Box", GUILayout.Width(320));
            GUILayout.Label(_m.GetInfo);
            GUILayout.EndVertical();
        }
    }
}