using UnityEngine;

namespace Core.Movement
{
    public struct BodyOrigins
    {
        /*public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
        public Vector2 top, bottom;
        public Vector2 left, right;*/
        public Vector2 center;
    }

    [RequireComponent(typeof(Collider2D))]
    public class Body2D : MonoBehaviour
    {
        protected Collider2D c;
        private BodyOrigins _o;

        public float skinWidth = .01f;

        /*public Vector2 TopLeft => _o.topLeft;
        public Vector2 TopRight => _o.topRight;
        public Vector2 BottomLeft => _o.bottomLeft;
        public Vector2 BottomRight => _o.bottomRight;
        public Vector2 Top => _o.top;
        public Vector2 Bottom => _o.bottom;
        public Vector2 Left => _o.left;
        public Vector2 Right => _o.right;*/
        protected Vector2 Center => _o.center;
        protected Vector2 Size { get; private set; }
        
        public Collider2D Collider => c;

        public void Awake()
        {
            c = GetComponent<Collider2D>();
        }

        protected void UpdateOrigins()
        {
            var bounds = c.bounds;
            // bounds.Expand(-2 * skinWidth);

            Size = bounds.size;

            /*_o.top = new Vector2(bounds.center.x, bounds.max.y);
            _o.bottom = new Vector2(bounds.center.x, bounds.min.y);
            _o.left = new Vector2(bounds.min.x, bounds.center.y);
            _o.right = new Vector2(bounds.max.x, bounds.center.y);*/
            _o.center = bounds.center;

            /*_o.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            _o.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            _o.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            _o.topRight = new Vector2(bounds.max.x, bounds.max.y);*/
        }
    }
}