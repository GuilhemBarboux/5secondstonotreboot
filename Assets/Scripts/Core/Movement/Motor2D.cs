using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Core.Movement
{
    public struct CollisionInfo
    {
        // Collision flags	
        public bool bottom, top;
        public bool left, right;
        public bool crushed;

        // Ground Info
        public Vector2 groundNormal;

        // Slope info
        public bool sliding;
        public bool descending, ascending;
        public float slopeAngle;

        // Wall info
        public Vector2 wallNormal;
        
        // Edge info

        /// <summary>
        ///     Reset the fields values to the default.
        /// </summary>
        public void Reset(Vector2 up)
        {
            bottom = top = false;
            left = right = false;
            crushed = false;

            groundNormal = up;

            sliding = false;
            descending = ascending = false;
            slopeAngle = 0;

            wallNormal = Vector2.zero;
        }

        public string GetInfoString()
        {
            return string.Concat(
                "Ground Info : \n",
                "Bottom = " + bottom + '\n',
                "Top = " + top + '\n',
                "Left = " + left + '\n',
                "Right = " + right + "\n",
                "Crushed = " + crushed + "\n\n",
                "Ground Info : \n",
                "GroundNormal = " + groundNormal + "\n\n",
                "Slopes Info : \n",
                "Sliding = " + sliding + '\n',
                "Descending = " + descending + '\n',
                "Ascending = " + ascending + '\n',
                "SlopeAngle = " + slopeAngle + "\n\n",
                "Wall Info : \n",
                "WallNormal = " + wallNormal
            );
        }
    }

    [RequireComponent(typeof(Collision2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class Motor2D : MonoBehaviour
    {
        private Collision2D _c;
        private Movement2D _m;
        private CollisionInfo _collisions;
        
        // Movement
        protected Vector2 v; // Velocity ref by world rotation (External update)
        private Vector2 _rv;  // v * Slope rotation * Motor rotation * deltaTime

        // Orientation
        private Vector2 _up;
        private Vector2 _right;
        private float _angle;
        private Quaternion _rotation;
        private Quaternion _slopeRotation;
        
        // Debugging
        [HideInInspector] public bool disableMovement;
        [HideInInspector] public bool disableCollision;
        [HideInInspector] public bool disableSlope;
        [HideInInspector] public bool disableRotation;
        
        // Hits
        [HideInInspector] public RaycastHit2D[] slidingHits = new RaycastHit2D[0];
        // [HideInInspector] public RaycastHit2D[] descendingHits = new RaycastHit2D[0];
        [HideInInspector] public RaycastHit2D[] ascendingHits = new RaycastHit2D[0];
        [HideInInspector] public RaycastHit2D[] wallHits = new RaycastHit2D[0];
        [HideInInspector] public RaycastHit2D[] groundHits = new RaycastHit2D[0];
        [HideInInspector] public RaycastHit2D[] ceilingHits = new RaycastHit2D[0];
        
        #region Variables

        public float maxSlope = 50f;
        public LayerMask groundMask;
        public string GetInfo => _collisions.GetInfoString();
        protected CollisionInfo Collisions => _collisions;
        public Vector2 Velocity => v;

        #endregion
        
        #region MonoBehavior

        protected void Awake()
        {
            // Initialize components
            _c = GetComponent<Collision2D>();
            _m = new Movement2D(GetComponent<Rigidbody2D>());
            
            // Initialize collisions
            _c.SetMask(groundMask);
            
            // Init rotation
            var transform1 = transform;
            _up = transform1.up;
            _right = transform1.right;
        }

        public void UpdateBehaviour()
        {
            var deltaVelocity = v * Time.deltaTime;
            
            // Calculate collisions
            if (!disableCollision) UpdateCollisions(deltaVelocity);
            
            // Update velocity rotation
            if (!disableSlope) UpdateSlope();
            if (!disableRotation) UpdateRotation();
            
            // Stop next movement on collision
            if (_collisions.top && v.y > 0f) v.y = 0f;
            if (_collisions.bottom && v.y < 0f) v.y = 0f;
            if (_collisions.right && v.x > 0f) v.x = 0f;
            if (_collisions.left && v.x < 0f) v.x = 0f;
            
            // Relative velocity
            _rv = _rotation * (_slopeRotation * deltaVelocity);
            if (!disableMovement) UpdatePosition(_rv);
        }

        #endregion

        #region Velocity

        public void AddVelocity(Vector2 movement)
        {
            v += movement;
        }

        public void SetVelocity(Vector2 movement)
        {
            v = movement;
        }

        #endregion

        #region Collisions

        public void CheckGround(IEnumerable<RaycastHit2D> verticalHits)
        {
            groundHits = verticalHits
                .Where(hit => Vector2.Angle(hit.normal, _up) <= maxSlope)
                .ToArray();

            if (!groundHits.Any()) return;
            
            _collisions.bottom =
                groundHits.Aggregate(false, (current, hit) => _c.IsTouching(hit.collider) || current);

            if (!_collisions.bottom) return;
            
            _collisions.groundNormal = groundHits.Aggregate(Vector2.zero, (c, r) => c + r.normal);
            _collisions.slopeAngle = Vector2.SignedAngle(_up, _collisions.groundNormal);
        }

        public void CheckSlope(IEnumerable<RaycastHit2D> horizontalHits, int directionX)
        {
            // Check next slope
            ascendingHits = horizontalHits
                .Where(hit => {
                    var angle = Vector2.Angle(hit.normal, _up);
                    return angle > 0 && angle <= maxSlope;
                })
                .ToArray();

            if (ascendingHits.Any())
            {
                _collisions.groundNormal = ascendingHits.Aggregate(Vector2.zero, (c, r) => c + r.normal);
                _collisions.slopeAngle = Vector2.SignedAngle(_up, _collisions.groundNormal);
            }
            
            if (!(Mathf.Abs(_collisions.slopeAngle) > 0f)) return;
            
            _collisions.ascending = Math.Sign(_collisions.slopeAngle) != directionX;
            _collisions.descending = Math.Sign(_collisions.slopeAngle) == directionX;
        }

        public void CheckSlide(IEnumerable<RaycastHit2D> verticalHits)
        {
            // Check Sliding
            slidingHits = verticalHits
                .Where(hit => Vector2.Angle(hit.normal, _up) > maxSlope)
                .ToArray();

            if (!slidingHits.Any()) return;
            
            var slidingNormal = slidingHits.Aggregate(Vector2.zero, (c, r) => c + r.normal);
            var slidingAngle = Vector2.SignedAngle(_up, slidingNormal);
            _collisions.slopeAngle = slidingAngle + Math.Sign(slidingAngle) * -90;
            _collisions.sliding = true;
        }

        public void CheckWall(IEnumerable<RaycastHit2D> horizontalHits, int directionX)
        {
            wallHits = horizontalHits
                .Where(hit => Vector2.Angle(hit.normal, _up) > maxSlope)
                .ToArray();

            if (!wallHits.Any()) return;
            if (!wallHits.Aggregate(false, (current, hit) => _c.IsTouching(hit.collider) || current)) return;
            
            _collisions.left = directionX < 0;
            _collisions.right = directionX > 0;
            _collisions.wallNormal = wallHits.Aggregate(Vector2.zero, (c, r) => c + r.normal);
        }

        public void CheckCeil(IEnumerable<RaycastHit2D> verticalHits)
        {
            ceilingHits = verticalHits
                .Where(hit => Vector2.Angle(hit.normal, _up) > maxSlope)
                .ToArray();
            
            if (ceilingHits.Any())
            {
                _collisions.top = true; // ceilingHits.Aggregate(false, (current, hit) => _c.IsTouching(hit.collider) || current);
                _collisions.crushed = _collisions.top && _collisions.bottom;
            }
        }

        protected void ResetCollisions()
        {
            _c.UpdateOrigins();
            _collisions.Reset(_up);
        }

        public void UpdateCollisions(Vector2 velocity)
        {
            ResetCollisions();

            var directionY = Math.Sign(velocity.y);
            var directionX = Math.Sign(velocity.x);
            var verticalHits = _c.GetHits( _up * directionY, velocity.y, _angle);
            var horizontalHits = _c.GetHits(_right * directionX, velocity.x, _angle);

            // Check Ground (Collider touching and below max slope)
            if (Mathf.Sign(velocity.y) <= 0f)
            {
                CheckGround(verticalHits);

                if (_collisions.bottom) CheckSlope(horizontalHits, directionX);
                else CheckSlide(verticalHits);
            }
            
            // Check Ceil
            else
            {
                CheckCeil(verticalHits);
            }
            
            // Check Wall (Collider touching and above max slope)
            CheckWall(horizontalHits, directionX);
        }
        
        #endregion

        #region Movement

        private void UpdateSlope()
        {
            if (_collisions.left || _collisions.right)
            {
                _slopeRotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                _slopeRotation = Quaternion.Euler(0, 0, _collisions.slopeAngle);
            }
        }

        private void UpdateRotation()
        {
            var angle = Vector2.SignedAngle(Vector2.up, transform.up);

            if (Math.Abs(angle - _angle) < 0.1f) return;
            
            var transform1 = transform;
            
            _angle = angle;
            _up = transform1.up;
            _right = transform1.right;
            _rotation = Quaternion.Euler(0, 0, _angle);
        }
        
        private void UpdatePosition(Vector2 velocity)
        {
            _m.UpdatePosition(velocity);
        }

        public void Teleport(Vector2 position)
        {
            _m.Teleport(position);
        }

        #endregion
        
        #region Debug

        protected void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;
            
            var position = transform.position;

            Gizmos.color = Color.white;
            Gizmos.DrawRay(position, _rotation * _rv.normalized);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(position, _rotation * _rv);

            Gizmos.color = Color.white;
            Gizmos.DrawRay(position, Collisions.groundNormal.normalized);
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(position, Collisions.groundNormal);

            foreach (var hit in slidingHits)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawRay(hit.point, hit.normal * 0.5f);
            }

            /*foreach (var hit in descendingHits)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawRay(hit.point, hit.normal * 0.6f);
            }*/

            foreach (var hit in ascendingHits)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawRay(hit.point, hit.normal * 0.7f);
            }

            foreach (var hit in wallHits)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawRay(hit.point, hit.normal * 0.8f);
            }

            foreach (var hit in groundHits)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawRay(hit.point, hit.normal * 0.9f);
            }

            foreach (var hit in ceilingHits)
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawRay(hit.point, hit.normal);
            }
        }

        #endregion
    }
}