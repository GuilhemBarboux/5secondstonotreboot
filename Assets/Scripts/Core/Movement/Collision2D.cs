﻿﻿using System;
using System.Linq;
using UnityEngine;

namespace Core.Movement
{
    public class Collision2D : Body2D
    {
        private LayerMask _collisionMask;
        private const int BufferSize = 10;
        protected RaycastHit2D[] bufferHits;

        public void Start()
        {
            UpdateOrigins();
            bufferHits = new RaycastHit2D[BufferSize];
        }
        
        public new void UpdateOrigins()
        {
            if (Application.isEditor) bufferHits = new RaycastHit2D[BufferSize];
            base.UpdateOrigins();
        }

        public void SetMask(LayerMask mask)
        {
            _collisionMask = mask;
        }

        public RaycastHit2D[] GetHits(Vector2 direction, float distance, float angle = 0f)
        {
            var count = CastFromCenter(
                angle,
                direction,
                skinWidth + Mathf.Abs(distance),
                _collisionMask,
                Vector2.zero);
            
            return bufferHits
                .Take(count)
                .Where(hit => Vector2.Angle(hit.normal, direction) > 90)
                .ToArray();
        }
        

        public RaycastHit2D[] GetVerticalHits(Vector2 velocity)
        {
            var directionY = transform.up * Mathf.Sign(velocity.y);
            var count = CastFromCenter(
                0f,
                directionY,
                skinWidth + Mathf.Abs(velocity.y),
                _collisionMask,
                Vector2.zero);
            
            return bufferHits
                .Take(count)
                .Where(hit => Vector2.Angle(hit.normal, -directionY) < 90)
                .ToArray();
        }

        public RaycastHit2D[] GetHorizontalHits(Vector2 velocity)
        {
            var directionX = transform.right * Mathf.Sign(velocity.x);
            var count = CastFromCenter(
                0f,
                directionX,
                skinWidth + Mathf.Abs(velocity.x),
                _collisionMask,
                Vector2.zero);
            
            return bufferHits
                .Take(count)
                .Where(hit => Vector2.Angle(hit.normal, -directionX) < 90)
                .ToArray();
        }

        protected virtual int CastFromCenter(float angle, Vector2 direction, float distance, LayerMask mask, Vector2 offset)
        {
            return Physics2D.BoxCastNonAlloc(
                Center + offset,
                Size,
                angle,
                direction,
                bufferHits,
                distance,
                mask);
        }

        public bool IsTouching(Collider2D coll2D)
        {
            // return c.Distance(coll2D).distance <= skinWidth;
            return c.IsTouching(coll2D);
        }
    }
}