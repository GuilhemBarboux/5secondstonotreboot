using Core.Helpers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Game
{
    public delegate void SceneFinish();
    public delegate void GameOver();
    public delegate void SceneStart();
    
    public class GameManager<T> : GameManager where T : GameManager
    {
        public new static T Instance => (T) GetInstance();
    }
    
    public class GameManager : SingletonMonoBehaviour<GameManager>
    {
        public static SceneFinish onSceneFinish;
        public static SceneFinish onSceneStart;

        protected new void Awake()
        {
            base.Awake();
            
            onSceneStart += D;
            onSceneFinish += D;
        }
        
        protected void Start()
        {
            onSceneStart();
        }

        public static void NextScene(string sceneName)
        {
            onSceneFinish();
            SceneManager.LoadScene(sceneName);
        }

        public static void D()
        {
            Debug.Log("event");
        }
    }
}