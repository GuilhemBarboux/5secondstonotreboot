using System;
using UnityEngine;

namespace Core.Game
{
    public class SceneTransition : MonoBehaviour
    {
        private Animator _animator;
        
        private int _transitionOut;
        private int _transitionIn;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            
            // Animator parameters
            _transitionOut = Animator.StringToHash("Out");
            _transitionIn = Animator.StringToHash("In");
            
            // Events
            GameManager.onSceneFinish += SceneTransitionOut;
            GameManager.onSceneStart += SceneTransitionIn;
        }

        private void SceneTransitionOut()
        {
            _animator.SetTrigger(_transitionOut);
        }

        private void SceneTransitionIn()
        {
            _animator.SetTrigger(_transitionIn);
        }
    }
}