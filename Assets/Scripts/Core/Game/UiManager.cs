using Core.Helpers;
using UnityEngine;

namespace Core.Game
{
    public class UiManager<T> : UiManager where T : UiManager
    {
        public new static T Instance => (T) GetInstance();
    }
    
    public class UiManager : SingletonMonoBehaviour<UiManager> {}
}