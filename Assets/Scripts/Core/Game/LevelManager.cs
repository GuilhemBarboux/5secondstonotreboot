using Core.Helpers;
using UnityEngine;

namespace Core.Game
{
    public class LevelManager<T> : LevelManager where T : LevelManager
    {
        public new static T Instance => (T) GetInstance();
    }
    
    public class LevelManager : SingletonMonoBehaviour<LevelManager> {}
}