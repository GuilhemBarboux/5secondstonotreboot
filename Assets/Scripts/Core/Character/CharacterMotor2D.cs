using System;
using System.Linq;
using Core.Movement;
using UnityEngine;

namespace Core.Character
{
    public class CharacterMotor2D : Motor2D
    {
        public CharacterData config;
        
        // Calculates variables
        private float _g;
        protected float maxJumpVelocity;
        private float _minJumpVelocity;

        protected void Start()
        {
            _g = -(2 * config.maxJumpHeight) / Mathf.Pow(config.maxJumpDuration, 2);
            maxJumpVelocity = Mathf.Abs(_g) * config.maxJumpDuration;
            _minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(_g) * config.minJumpHeight);
        }
        
        #region States
        
        public bool Grounded()
        {
            return Collisions.bottom;
        }
        
        public bool Falling()
        {
            return !Grounded() && !Collisions.sliding;
        }
        
        public bool Sliding()
        {
            return !Grounded() && Collisions.sliding;
        }

        public bool CanJump()
        {
            return Grounded();
        }

        public bool CanGrab()
        {
            return true;
        }

        #endregion
        
        #region Actions
        
        public void JumpStart(float jumpScale = 1f)
        {
            if (!Collisions.bottom || Collisions.sliding) return;
            v.y = maxJumpVelocity * jumpScale;
        }

        public void JumpEnd()
        {
            if (v.y > _minJumpVelocity) v.y = _minJumpVelocity;
        }
        
        public void GroundMovement(float horizontalValue, float speedScale = 1f)
        {
            var isMoving = horizontalValue > 0f || horizontalValue < 0f;
            var targetVelocityX = isMoving ? horizontalValue * config.maxMoveSpeed * speedScale: 0f;
            var acceleration = isMoving ? config.groundAcceleration : config.groundDeceleration;
            
            v.x = Mathf.MoveTowards(v.x, targetVelocityX, acceleration * Time.deltaTime);
            v.y += _g * Time.deltaTime;
        }

        public void AirMovement(float horizontalValue)
        {
            var isMoving = horizontalValue > 0f || horizontalValue < 0f;
            var targetVelocityX = isMoving ? horizontalValue * config.maxMoveSpeed : 0f;
            var acceleration = isMoving ? config.airborneAcceleration : config.airborneDeceleration;
            
            v.x = Mathf.MoveTowards(v.x, targetVelocityX, acceleration * Time.deltaTime);
            v.y += _g * Time.deltaTime;

            if (Mathf.Abs(v.y) > config.maxFallSpeed)
                v.y = Mathf.Sign(v.y) * config.maxFallSpeed;
        }

        public void SlideMovement()
        {
            v.x = 0f;
            v.y += _g * Time.deltaTime;
        }

        public void CrunchMovement(float horizontalValue)
        {
            GroundMovement(horizontalValue, config.crunchScale);
        }

        public void CrunchJumpStart()
        {
            JumpStart(config.crunchJumpBoost);
        }

        #endregion
    }
}