using UnityEngine;
using Collision2D = Core.Movement.Collision2D;

namespace Core.Character
{
    public class CharacterCollision2D : Collision2D
    {
        protected override int CastFromCenter(float angle, Vector2 direction, float distance, LayerMask mask, Vector2 offset)
        {
            var caps = (CapsuleCollider2D) c;
            
            return Physics2D.CapsuleCastNonAlloc(
                Center + offset,
                Size,
                caps.direction,
                angle,
                direction,
                bufferHits,
                distance,
                mask);
        }
    }
}