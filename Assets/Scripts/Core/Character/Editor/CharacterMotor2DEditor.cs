using Core.Movement;
using UnityEditor;
using UnityEngine;

namespace Core.Character
{
    [CustomEditor(typeof(CharacterMotor2D))]
    public class CharacterMotor2DEditor : Motor2DEditor
    {
        private CharacterMotor2D _m;

        private void OnEnable()
        {
            if (_m == null) _m = (CharacterMotor2D) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.BeginVertical("Box", GUILayout.Width(320));
            GUILayout.Label(string.Concat(
                "Grounded = " + _m.Grounded() + '\n',
                "Falling = " + _m.Falling() + '\n',
                "Sliding = " + _m.Sliding() + '\n',
                "CanJump = " + _m.CanJump() + '\n',
                "CanGrab = " + _m.CanGrab()
            ));
            GUILayout.EndVertical();
        }
    }
}