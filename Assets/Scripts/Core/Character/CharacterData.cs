using UnityEngine;

namespace Core.Character
{
    [CreateAssetMenu(fileName = "Character data", menuName = "Core/Character data", order = 0)]
    public class CharacterData : ScriptableObject
    {
        [Header("Jump control")]
        public float maxJumpHeight = 4f;
        public float minJumpHeight = 1f;
        public float maxJumpDuration = .4f;
        public float coyoteDuration = .05f;
        
        [Header("Ground control")]
        public float maxMoveSpeed = 6f;
        public float groundAcceleration = 100f;
        public float groundDeceleration = 100f;
        
        [Header("Air control")]
        public float airborneAcceleration = 100f;
        public float airborneDeceleration = 50f;
        public float maxFallSpeed = 25f;

        [Header("Crunch control")]
        public float crunchScale = 0.33f;
        public float crunchJumpBoost = 2.5f;
        
        [Header("Fall through")]
        public LayerMask platformMask;
    }
}