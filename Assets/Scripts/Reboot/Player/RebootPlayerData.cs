using UnityEngine;

namespace Reboot.Player
{
    [CreateAssetMenu(fileName = "RebootPlayerData", menuName = "Reboot Player Data", order = 0)]
    public class RebootPlayerData : ScriptableObject
    {
        [Header("Movement Properties")]
        public float speed = 8f;				//Player speed
        public float groundAcceleration = 100f;
        public float groundDeceleration = 100f;
        public float crouchSpeedScale = 3f;	//Speed reduction when crouching
        public float coyoteDuration = .05f;		//How long the player can jump after falling
        public float maxFallSpeed = -25f;		//Max speed player can fall
        public float wallStickDuration = .2f;		//Max speed player can fall
        public float wallStickForce = .2f;

        [Header("Jump Properties")]
        public float jumpForce = 4f;
        public float jumpDuration = .4f;
        public float jumpHoldForce = 1f;		//Incremental force when jump is held
        public float jumpHoldDuration = .1f;	//How long the jump key can be held
        public float wallJumpDuration = .3f;	//How long the jump key can be held
        // public float wallJumpForce = 2f;	//How long the jump key can be held
        public float crouchJumpBoost = 2.5f;	//Jump boost when crouching
        public float hangingJumpForce = 15f;	//Force of wall hanging jumo

        [Header("Air control")]
        public float airborneAcceleration = 100f;
        public float airborneDeceleration = 50f;
        
        [Header("Environment Check Properties")]
        public float footOffset = .4f;			//X Offset of feet raycast
        public float eyeHeight = 1.5f;			//Height of wall checks
        public float reachOffset = .7f;			//X offset for wall grabbing
        public float headClearance = .5f;		//Space needed above the player's head
        public float groundDistance = .2f;		//Distance player is considered to be on the ground
        public float grabDistance = .4f;		//The reach distance for wall grabs
        public LayerMask groundLayer;		//The reach distance for wall grabs
        public LayerMask platformLayer;
        public string pushTag = "Pushable";
    }
}