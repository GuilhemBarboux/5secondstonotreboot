using Core.StateMachine;
using UnityEngine;

namespace Reboot.Player.States
{
    public class Pause : RebootPlayerState
    {
        private bool _isReseting;
        
        public Pause(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController) {}

        public override void PhysicUpdate()
        {
            if (_isReseting) return;
            
            _isReseting = true;
            var copy = playerController.transform;
            var deadPosition = copy.position;
            var deadScale = copy.localScale;
                
            Motor.ResetPosition(playerController.InitialPosition);
            playerController.AddDeadBody(deadPosition, deadScale);
        }

        public override void Exit()
        {
            base.Exit();
            _isReseting = false;
        }
    }
}