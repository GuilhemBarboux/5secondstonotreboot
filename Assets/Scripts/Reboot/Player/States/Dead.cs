using Core.StateMachine;
using UnityEngine;

namespace Reboot.Player.States
{
    public class Dead : RebootPlayerState
    {
        public Dead(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController) {}

        public override void Enter()
        {
            base.Enter();
            Motor.SetVelocity(Vector2.zero);
        }

        /*public override void PhysicUpdate()
        {
            if (!Motor.isOnWall && !Motor.isOnGround)
            {
                Motor.AirMovement(0f);
            }
            
            base.PhysicUpdate();
        }*/
    }
}