using Core.StateMachine;

namespace Reboot.Player.States
{
    public class Crunch : RebootPlayerState
    {
        public Crunch(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController)
        {
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();

            if (Inputs.jump)
            {
                ChangeToState(playerController.AirborneState);
            }
            else if (!Inputs.crunch)
            {
                ChangeToState(playerController.LocomotionState);
            }
        }

        public override void PhysicUpdate()
        {
            Motor.CheckGround();
            Motor.CheckHead();
            Motor.CheckWall();
            
            Motor.CrunchMovement(Inputs.direction.x);
            
            base.PhysicUpdate();
        }
    }
}