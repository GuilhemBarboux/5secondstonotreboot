using Core.StateMachine;
using UnityEngine;

namespace Reboot.Player.States
{
    public class Wall : RebootPlayerState
    {
        public Wall(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController)
        {
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            
            if (WasWallJump)
            {
                Motor.StartWallJump();
                playerController.isJumping = true;
                ChangeToState(playerController.AirborneState);
            }
            else if (Motor.isOnGround)
            {
                ChangeToState(playerController.LocomotionState);
            }
            else if (!Motor.isOnWall && !Motor.CanWallJump())
            {
                ChangeToState(playerController.AirborneState);
            }
        }

        public override void PhysicUpdate()
        {
            Motor.CheckGround();
            Motor.CheckHead();
            Motor.CheckWall();

            Motor.AirMovement(Inputs.direction.x);
            Motor.UpdateWallStick();

            Motor.UpdateCharacterDirection();
            
            base.PhysicUpdate();
        }
    }
}