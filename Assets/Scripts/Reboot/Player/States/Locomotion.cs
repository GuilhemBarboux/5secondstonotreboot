using Core.StateMachine;

namespace Reboot.Player.States
{
    public class Locomotion : RebootPlayerState
    {
        public Locomotion(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController)
        {
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            
            if (WasJump)
            {
                Motor.StartJump();
                playerController.isJumping = true;
                ChangeToState(playerController.AirborneState);
            }
            else if (!Motor.isOnGround && !Motor.CanJump())
            {
                ChangeToState(playerController.AirborneState);
            }
        }

        public override void PhysicUpdate()
        {
            Motor.CheckGround();
            Motor.CheckHead();
            Motor.CheckWall();
            
            Motor.GroundMovement(Inputs.direction.x);
            
            Motor.UpdateCharacterDirection();
            
            base.PhysicUpdate();
        }
    }
}