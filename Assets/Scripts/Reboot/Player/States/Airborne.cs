using Core.StateMachine;
using UnityEngine;

namespace Reboot.Player.States
{
    public class Airborne : RebootPlayerState
    {
        public Airborne(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController)
        {
        }

        public override void Enter()
        {
            base.Enter();
            Motor.ResetGroundAndWall();
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if (Motor.isOnGround) ChangeToState(playerController.LocomotionState);
            if (Motor.isOnWall) ChangeToState(playerController.WallState);
        }

        public override void PhysicUpdate()
        {
            if (Motor.Velocity.y <= 0f) Motor.CheckGround();
            
            Motor.CheckHead();
            Motor.CheckWall();
            
            Motor.AirMovement(Inputs.direction.x);
            Motor.UpdateJump(Inputs.jump);

            Motor.UpdateCharacterDirection();
            
            base.PhysicUpdate();
        }
    }
}