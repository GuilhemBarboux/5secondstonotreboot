using Core.StateMachine;
using UnityEngine;

namespace Reboot.Player.States
{
    public class Ready : RebootPlayerState
    {
        private bool _start;
        
        public Ready(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner, rebootPlayerController) {}

        public override void LogicUpdate()
        {
            base.LogicUpdate();

            if (!Inputs.crunch && !Inputs.jump && (!(Inputs.direction.magnitude > 0f) || _start)) return;
            
            _start = true;
            RebootPlayerController.StartReboot();
        }
        
        public override void PhysicUpdate()
        {
            Motor.SetVelocity(Vector2.zero);
        }

        public override void Exit()
        {
            base.Exit();
            _start = false;
        }
    }
}