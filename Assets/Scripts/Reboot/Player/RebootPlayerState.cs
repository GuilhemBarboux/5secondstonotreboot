using Core.Player;
using Core.StateMachine;

namespace Reboot.Player
{
    public class RebootPlayerState : StateMachine<RebootPlayerState>
    {
        protected readonly RebootPlayerController playerController;
        protected RebootPlayerMotor Motor => playerController.Motor;
        protected PlayerControllerInputs Inputs => playerController.inputs;

        protected bool WasJump => Inputs.jump && Motor.CanJump() && !playerController.isJumping;
        protected bool WasWallJump => Inputs.jump && Motor.CanWallJump() && !playerController.isJumping;
        
        protected RebootPlayerState(string name, Machine<RebootPlayerState> owner, RebootPlayerController rebootPlayerController) : base(name, owner)
        {
            playerController = rebootPlayerController;
        }

        public virtual void LogicUpdate() { }

        public virtual void PhysicUpdate()
        {
            Motor.UpdatePosition();
        }
    }
}