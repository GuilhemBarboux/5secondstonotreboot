using System;
using Core.Character;
using Core.Movement;
using UnityEngine;

namespace Reboot.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public class RebootPlayerMotor : Body2D
    {
        public bool drawDebugRaycasts = true;
        public RebootPlayerData data;

        [Header ("Status Flags")]
        public bool isOnGround;
        public bool isOnWall;
        public bool isOnHead;
        public bool isOnWallStick;
        public bool isOnPush;
        public Vector2 Velocity => new Vector2(_velocity.x / data.speed, _velocity.y / _jumpSpeed);
        public float Gravity => _gravity;
        public float Direction => Mathf.Sign(_scaleX);
        
        [Header ("Movement")]
        private Movement2D _movement;
        private float _scaleX = 1;
        private Vector2 _velocity;
        private Collider2D _collider;
        
        [Header("Airborne")]
        private float _gravity;
        private float _jumpSpeed;
        private float _jumpHoldSpeed;
        private Vector2 _jumpDirection;
        private float _jumpTime;
        private float _coyoteTime;
        private float _wallCoyoteTime;
        // private float _wallTime;
        private float _wallJumpTime;
        private float _wallDirection;

        protected new void Awake()
        {
            base.Awake();
            
            _collider = GetComponent<Collider2D>();
            _movement = new Movement2D(GetComponent<Rigidbody2D>());
            _gravity = -(2 * (data.jumpForce + data.jumpHoldForce)) / Mathf.Pow(data.jumpDuration, 2);
            _jumpSpeed = Mathf.Abs(_gravity) * data.jumpDuration;
            _jumpHoldSpeed = 2 * data.jumpHoldForce / Mathf.Pow(data.jumpDuration, 2);
        }

        protected void Start()
        {
            _scaleX = transform.localScale.x;
        }

        private void Update()
        {
            if (isOnGround || isOnWall) _wallJumpTime = Time.time;
            if (isOnGround) _coyoteTime = Time.time + data.coyoteDuration;
            if (isOnWall) _wallCoyoteTime = Time.time + data.coyoteDuration;
        }

        public void UpdatePosition()
        {
            var deltaVelocity = _velocity * Time.deltaTime;
            _movement.UpdatePosition(deltaVelocity);
            if (drawDebugRaycasts) Debug.DrawRay(transform.position, deltaVelocity);
        }

        public void UpdateCharacterDirection()
        {
            if (_velocity.x * _scaleX >= 0f) return;
            FlipCharacterDirection();
        }

        private void FlipCharacterDirection()
        {
            var transform1 = transform;
            var scale = transform1.localScale;
            _scaleX *= -1;
            scale.x = _scaleX;
            transform1.localScale = scale;
        }

        public void ResetGroundAndWall()
        {
            isOnGround = false;
            isOnWall = false;
        }

        public void ResetPosition(Vector2 p)
        {
            transform.position = p;
            if (_scaleX < 0f) FlipCharacterDirection();
        }

        public void SetVelocity(Vector2 v)
        {
            _velocity = v;
        }

        public bool CanJump()
        {
            return isOnGround || _coyoteTime > Time.time;
        }
        
        public bool CanWallJump()
        {
            return isOnWall || _wallCoyoteTime > Time.time;
        }
        
        public void StartJump()
        {
            _velocity.y = _jumpSpeed;
            _jumpTime = Time.time + data.jumpHoldDuration;
            _jumpDirection = Vector2.up;
        }
        
        public void StartWallJump()
        {
            var directionX = Mathf.Sign(_wallDirection);
            _velocity = new Vector2(-directionX * _jumpSpeed * 1.5f, _jumpSpeed);
            _jumpTime = Time.time + data.jumpHoldDuration;
            _wallJumpTime = Time.time + data.wallJumpDuration;
            _jumpDirection = _velocity.normalized;
            FlipCharacterDirection();
        }

        public void UpdateJump(bool held)
        {
            if (!isOnGround && !isOnWall && !isOnHead)
            {
                if (held && _jumpTime > Time.time)
                {
                    _velocity += _jumpDirection * (_jumpHoldSpeed * Time.deltaTime);
                }
            
                if (_wallJumpTime > Time.time)
                {
                    _velocity.x = -Math.Sign(_wallDirection) * Mathf.Abs(_velocity.x);
                }
            }

            if (isOnHead && _velocity.y > 0f) _velocity.y = 0f;
        }

        public void UpdateWallStick()
        {
            if (isOnWall && _velocity.x * _scaleX > 0f && !isOnGround && _velocity.y <= 0f)
            {
                isOnWallStick = true;
            }
            else
            {
                isOnWallStick = false;
            }
            
            if (isOnWallStick) _velocity.y = _gravity * Time.deltaTime * data.wallStickForce;
        }

        public void AirMovement(float horizontalValue)
        {
            var isMoving = horizontalValue > 0f || horizontalValue < 0f;
            var targetVelocityX = isMoving ? horizontalValue * data.speed : 0f;
            var acceleration = isMoving ? data.airborneAcceleration : data.airborneDeceleration;
            
            _velocity.x = Mathf.MoveTowards(_velocity.x, targetVelocityX, acceleration * Time.deltaTime);
            _velocity.y += _gravity * Time.deltaTime;
            
            if (_velocity.y < data.maxFallSpeed) _velocity.y = data.maxFallSpeed;
        }

        public void GroundMovement(float horizontalValue, float speedScale = 1f)
        {
            var isMoving = horizontalValue > 0f || horizontalValue < 0f;
            var targetVelocityX = isMoving ? horizontalValue * data.speed * speedScale: 0f;
            var acceleration = isMoving ? data.groundAcceleration : data.groundDeceleration;
            
            _velocity.x = Mathf.MoveTowards(_velocity.x, targetVelocityX, acceleration * Time.deltaTime * speedScale);
            // _velocity.y = _gravity * Time.deltaTime;
        }

        public void CrunchMovement(float horizontalValue)
        {
            GroundMovement(horizontalValue, data.crouchSpeedScale);
        }
        
        #region Physics

        public void CheckGround()
        {
            var leftCheck = Raycast(new Vector2(-data.footOffset, 0f), Vector2.down, data.groundDistance, data.platformLayer);
            var rightCheck = Raycast(new Vector2(data.footOffset, 0f), Vector2.down, data.groundDistance, data.platformLayer);
            
            isOnGround = leftCheck && !_collider.bounds.Contains(leftCheck.point) || rightCheck && !_collider.bounds.Contains(rightCheck.point);
        }

        public void CheckHead()
        {
            var headCheck = Raycast(new Vector2(0f, Size.y), Vector2.up, data.headClearance);
            isOnHead = headCheck;
        }

        public void CheckWall() {
            var wallDirection = Mathf.Sign(_scaleX);
            var wallCheck = Raycast(new Vector2(data.footOffset * wallDirection, data.eyeHeight), new Vector2(wallDirection, 0f), data.grabDistance);
            isOnWall = wallCheck;
            _wallDirection = isOnWall ? wallDirection : -wallDirection;
        }

        public void CheckPush() {
            var pushDirection = Mathf.Sign(_scaleX);
            var pushCheck = Raycast(new Vector2(data.footOffset * pushDirection, data.eyeHeight), new Vector2(pushDirection, 0f), data.grabDistance);
            isOnPush = pushCheck && pushCheck.collider.CompareTag(data.pushTag) && _velocity.x * _scaleX > 0f;
        }

        private RaycastHit2D Raycast(Vector2 offset, Vector2 rayDirection, float length)
        {
            //Call the overloaded Raycast() method using the ground layermask and return 
            //the results
            return Raycast(offset, rayDirection, length, data.groundLayer);
        }

        private RaycastHit2D Raycast(Vector2 offset, Vector2 rayDirection, float length, LayerMask mask)
        {
            //Record the player's position
            var pos = (Vector2) transform.position;

            //Send out the desired raycasr and record the result
            var hit = Physics2D.Raycast(pos + offset, rayDirection, length, mask);
            
            //If we want to show debug raycasts in the scene...
            if (drawDebugRaycasts)
            {
                //...determine the color based on if the raycast hit...
                Color color = hit ? Color.red : Color.green;
                //...and draw the ray in the scene view
                Debug.DrawRay(pos + offset, rayDirection * length, color);
            }

            //Return the results of the raycast
            return hit;
        }

        #endregion
    }
}