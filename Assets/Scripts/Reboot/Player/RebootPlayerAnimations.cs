﻿using Reboot.Game;
using UnityEngine;

namespace Reboot.Player
{
    [RequireComponent(typeof(RebootPlayerController))]
    [RequireComponent(typeof(Animator))]
    public class RebootPlayerAnimations : MonoBehaviour
    {
        private Animator _animator;
        // private int _hangingParamId; //ID of the isHanging parameter
        private int _crouchParamId; //ID of the isCrouching parameter
        private int _fallParamId; //ID of the verticalVelocity parameter
        private int _groundParamId; //ID of the isOnGround parameter
        private int _speedParamId; //ID of the speed parameter
        private int _readyParamId;
        private int _deadParamId;
        private int _startParamId;
        private int _wallStickParamId;
        private int _pushParamId;
        private RebootPlayerController _controller;
        private SpriteRenderer[] _renderers;

        private void Start()
        {
            _controller = GetComponent<RebootPlayerController>();
            //Get the integer hashes of the parameters. This is much more efficient
            //than passing strings into the animator
            // _hangingParamId = Animator.StringToHash("isHanging");
            _groundParamId = Animator.StringToHash("isOnGround");
            _crouchParamId = Animator.StringToHash("isCrouching");
            _speedParamId = Animator.StringToHash("speedX");
            _fallParamId = Animator.StringToHash("speedY");
            _readyParamId = Animator.StringToHash("isReady");
            _deadParamId = Animator.StringToHash("isDead");
            _startParamId = Animator.StringToHash("isStart");
            _wallStickParamId = Animator.StringToHash("isWallStick");
            _pushParamId = Animator.StringToHash("isPush");

            //Get references to the needed components
            _animator = GetComponent<Animator>();
            
            // Renderers
            _renderers = GetComponentsInChildren<SpriteRenderer>();
        }

        private void Update()
        {
            // _animator.SetBool(_hangingParamId, motor.hanging);
            _animator.SetBool(_groundParamId, _controller.IsGrounded);
            _animator.SetBool(_crouchParamId, _controller.IsCrunch);
            _animator.SetBool(_readyParamId, _controller.IsReady);
            _animator.SetBool(_deadParamId, _controller.IsDead);
            _animator.SetBool(_startParamId, _controller.IsStart);
            _animator.SetFloat(_speedParamId, Mathf.Abs(_controller.Velocity.x));
            _animator.SetFloat(_fallParamId, Mathf.Clamp(_controller.Velocity.y, -1f, 1f));
            _animator.SetBool(_wallStickParamId, _controller.IsWallStick);
            _animator.SetBool(_pushParamId, _controller.IsPush);

            foreach (var spriteRenderer in _renderers)
            {
                spriteRenderer.enabled = _controller.IsReady;
            }
        }
        
        public void OnSpawnAnimation()
        {
            AudioManager.PlaySpawnAudio();
        }

        public void OnJumpAnimation()
        {
            AudioManager.PlayJumpAudio();
        }
        
        public void OnDeadAnimation()
        {
            AudioManager.PlayDeathAudio();
        }

        public void OnFootStepAnimation()
        {
            AudioManager.PlayFootstepAudio();
        }
    }
}