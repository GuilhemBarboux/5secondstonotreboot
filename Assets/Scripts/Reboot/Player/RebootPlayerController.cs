using System;
using System.Collections;
using Core.Player;
using Core.StateMachine;
using Reboot.Game;
using Reboot.Player.States;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Reboot.Player
{
    [RequireComponent(typeof(RebootPlayerMotor))]
    public class RebootPlayerController : PlayerController
    {
        public Vector2 InitialPosition { get; private set; }
        public GameObject bodyPrefab;
        
        // Movement
        public RebootPlayerMotor Motor { get; private set; }
        public bool paused;
        
        // Jump
        public bool isJumping;

        // States
        private Machine<RebootPlayerState> _machine;
        public Locomotion LocomotionState { get; private set; }
        public Airborne AirborneState { get; private set; }
        public Wall WallState { get; private set; }
        public Crunch CrunchState { get; private set; }
        public Ready ReadyState { get; private set; }
        public Pause ResetState { get; private set; }
        public Dead DeadState { get; private set; }

        // Status
        public bool IsGrounded => Motor.isOnGround;
        public bool IsCrunch => _machine.CurrentState == CrunchState;
        public bool IsPush => Motor.isOnPush;
        public bool IsWallStick => Motor.isOnWall && Motor.isOnWallStick;
        public bool IsDead { get; private set; }
        public bool IsReady { get; private set; }
        public bool IsStart { get; private set; }
        public bool IsCopy { get; private set; }
        public Vector2 Velocity => Motor.Velocity;
        
        #region MonoBehaviour
        
        private void Awake()
        {
            _machine = new Machine<RebootPlayerState>("RebootPlayer");
            Motor = GetComponent<RebootPlayerMotor>();
            InitialPosition = transform.position;
            
            LevelManager.onRebootStart += OnRebootStart;
            LevelManager.onRebootFinish += OnRebootFinish;
            LevelManager.onRebootReady += OnRebootReady;
        }

        protected void Start()
        {
            LocomotionState = new Locomotion("Locomotion", _machine, this);
            AirborneState = new Airborne("Airborne", _machine, this);
            WallState = new Wall("Wall", _machine, this);
            CrunchState = new Crunch("Crunch", _machine, this);
            ReadyState = new Ready("Ready", _machine, this);
            ResetState = new Pause("Reset", _machine, this);
            DeadState = new Dead("Dead", _machine, this);
            
            _machine.Start(DeadState);
            // _machine.Start(LocomotionState);
        }

        protected void Update()
        {
            _machine.CurrentState.LogicUpdate();
            if (!inputs.jump && isJumping) isJumping = false;
        }

        protected void FixedUpdate()
        {
            _machine.CurrentState.PhysicUpdate();
        }

        #endregion
        
        public static void StartReboot()
        {
            LevelManager.StartReboot();
        }

        public static void FinishReboot()
        {
            LevelManager.FinishReboot();
        }

        private void OnRebootStart()
        {
            _machine.ChangeToState(LocomotionState);
            IsStart = true;
        }

        private void OnRebootFinish()
        {
            _machine.ChangeToState(DeadState);
            IsStart = false;
            IsDead = true;
        }

        private void OnRebootReady()
        {
            _machine.ChangeToState(ReadyState);
            IsReady = true;
        }

        private void OnDead()
        {
            IsDead = false;
            IsReady = false;
            IsCopy = true;
            _machine.ChangeToState(ResetState);
        }

        public void AddDeadBody(Vector2 position, Vector2 scale)
        {
            var o = Instantiate(bodyPrefab, position, Quaternion.identity);
            o.transform.localScale = scale;
        }

        public override void OnMenu(InputValue value)
        {
            LevelManager.Pause();
        }
    }
}