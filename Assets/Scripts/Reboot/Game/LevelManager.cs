using System.Collections;
using System.Security.Cryptography;
using Core.Game;
using Reboot.Player;
using UnityEngine;

namespace Reboot.Game
{
    public delegate void RebootReady();
    public delegate void RebootStart();
    public delegate void RebootFinish();

    public class LevelManager : LevelManager<LevelManager>
    {
        public string nextSceneName;
        public int rebootDuration;
        public float rebootTransitionDuration;
        public bool paused;
        public LevelData data;

        private float _rebootTime;
        private int _rebootCount;
        private bool _rebootRun;
        private bool _rebootReady;

        public static RebootReady onRebootReady;
        public static RebootStart onRebootStart;
        public static RebootFinish onRebootFinish;
        
        private void Start()
        {
            // Init at start
            _rebootTime = 0;
            _rebootCount = 0;
            _rebootRun = false;
            _rebootReady = false;

            // Ready to play
            StartCoroutine(ReadyReboot());
            
            // Reset UI
            UiManager.UpdateCountUi(_rebootCount);
            UiManager.UpdateCountUi(rebootDuration);
            UiManager.UpdateReboot(data.maxReboot, data.maxReboot);
            
            // Audio Events
            AudioManager.PlaySceneRestartAudio();
        }

        private void Update()
        {
            if (_rebootRun)
            {
                _rebootTime += Time.deltaTime;
                if (_rebootTime > rebootDuration) FinishReboot();
            }

            UiManager.UpdateTimeUi(rebootDuration - Mathf.Max(_rebootTime, 0));
        }

        public static void Pause()
        {
            if (Time.timeScale > 0f)
            {
                Time.timeScale = 0f;
                UiManager.OnMenu();
            }
            else
            {
                UnPause();
            }
        }

        public static void UnPause()
        {
            Time.timeScale = 1f;
            UiManager.OnHideMenu();
        }

        public static void StartReboot()
        {
            if (Instance._rebootRun || !Instance._rebootReady) return;
            
            Instance._rebootRun = true;

            onRebootStart();
        }

        public static void FinishReboot()
        {
            if (!Instance._rebootRun) return;
            if (Instance.data.maxReboot - Instance._rebootCount == 0) UiManager.Instance.OnRestart(); 
            
            Instance._rebootCount++;
            Instance._rebootTime = 0f;
            Instance._rebootRun = false;
            Instance._rebootReady = false;
            
            UiManager.UpdateCountUi(Instance._rebootCount);
            UiManager.UpdateReboot(Instance.data.maxReboot - Instance._rebootCount, Instance.data.maxReboot);

            onRebootFinish();
            
            Instance.StartCoroutine(ReadyReboot());
        }

        private static IEnumerator ReadyReboot()
        {
            yield return new WaitForSeconds(Instance.rebootTransitionDuration);
            Debug.Log("Ready corou");
            Instance._rebootReady = true;
            
            onRebootReady();
        }

        public static void NextScene()
        {
            GameManager.NextScene(Instance.nextSceneName);
        }
    }
}