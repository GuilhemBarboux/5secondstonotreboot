using Core.Game;

namespace Reboot.Game
{
    public class GameManager : GameManager<GameManager>
    {
        public new static void NextScene(string sceneName)
        {
            Core.Game.GameManager.NextScene(sceneName);
        }
    }
}