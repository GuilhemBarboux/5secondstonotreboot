using UnityEngine;

namespace Reboot.Game
{
    [CreateAssetMenu(fileName = "Level data", menuName = "Level Data", order = 0)]
    public class LevelData : ScriptableObject
    {
        public int maxReboot = 3;
        public int threeStarReboot = 2;
        public int twoStarReboot = 1;
        public int oneStarReboot;
    }
}