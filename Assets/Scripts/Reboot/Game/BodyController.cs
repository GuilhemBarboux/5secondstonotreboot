using System;
using System.Linq;
using Reboot.Player;
using UnityEngine;

namespace Reboot.Game
{
    [RequireComponent(typeof(CapsuleCollider2D))]
    public class BodyController : MonoBehaviour
    {
        public float distance;
        public LayerMask groundLayerMask;
        public LayerMask trapLayerMask;

        private CapsuleCollider2D _collider;
        private bool _isFixed;
        private readonly CapsuleCollider2D[] _traps = new CapsuleCollider2D[1];
        private readonly RaycastHit2D[] _grounds = new RaycastHit2D[2];
        private void Awake()
        {
            _collider = GetComponent<CapsuleCollider2D>();
        }
        
        private void FixedUpdate()
        {
            if (_isFixed) return;
            
            var bound = _collider.bounds;
            bound.Expand(new Vector3(distance, distance / 2, 0f));
            var pos = transform.position - new Vector3(0, distance, 0);
            var size = Physics2D.OverlapCapsuleNonAlloc(pos, bound.size, CapsuleDirection2D.Vertical, 0f, _traps, trapLayerMask);
            
            if (size > 0) _isFixed = true;
            else
            {
                size = Physics2D.CapsuleCastNonAlloc(pos, _collider.bounds.size, CapsuleDirection2D.Vertical, 0f, Vector2.down,
                    _grounds, distance, groundLayerMask);
                
                if (size > 1)
                {
                    _isFixed = true;
                    transform.Translate(0, -_grounds.Max(h => h.distance), 0);
                }
                else
                {
                    transform.Translate(0, -distance, 0);
                }
            }
        }
    }
}