using System;
using Reboot.Player;
using UnityEngine;

namespace Reboot.Game
{
    public class DeadZone : MonoBehaviour
    {
        private RebootPlayerController _controller;
        
        private void Awake()
        {
            _controller = FindObjectOfType<RebootPlayerController>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject != _controller.gameObject) return;
            RebootPlayerController.FinishReboot();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject != _controller.gameObject) return;
            RebootPlayerController.FinishReboot();
        }
    }
}