using Core.Helpers;
using UnityEngine;
using UnityEngine.Audio;

namespace Reboot.Game
{
    public class AudioManager<T> : AudioManager where T : AudioManager
    {
        public new static T Instance => (T) GetInstance();
    }
    
    public class AudioManager : SingletonMonoBehaviour<AudioManager>
    {
        [Header("Ambient Audio")]
        public AudioClip ambientClip;
        public AudioClip musicClip;
        
        [Header("Stings")]
        public AudioClip levelStingClip;
        // public AudioClip deathStingClip;
        // public AudioClip winStingClip;
        
        [Header("Player")]
        public AudioClip[] walkStepClips;
        public AudioClip deathClip;
        public AudioClip jumpClip;
        public AudioClip spawnClip;
        
        [Header("Mixer Groups")]
        public AudioMixerGroup ambientGroup;
        public AudioMixerGroup musicGroup;
        public AudioMixerGroup stingGroup;
        public AudioMixerGroup playerGroup;
        
        private AudioSource _ambientSource;
        private AudioSource _musicSource;
        private AudioSource _stingSource;
        private AudioSource _playerSource;

        protected new void Awake()
        {
            base.Awake();

            _ambientSource = gameObject.AddComponent<AudioSource>();
            _musicSource = gameObject.AddComponent<AudioSource>();
            _stingSource = gameObject.AddComponent<AudioSource>();
            _playerSource = gameObject.AddComponent<AudioSource>();
            
            _ambientSource.outputAudioMixerGroup = ambientGroup;
            _musicSource.outputAudioMixerGroup	= musicGroup;
            _stingSource.outputAudioMixerGroup	= stingGroup;
            _playerSource.outputAudioMixerGroup	= playerGroup;
        }

        public static void StartMenuAudio()
        {
            Instance._ambientSource.Stop();
            
            Instance._musicSource.clip = Instance.musicClip;
            Instance._musicSource.loop = true;
            Instance._musicSource.Play();
        }
        
        public static void PlayFootstepAudio()
        {
            var index = Random.Range(0, Instance.walkStepClips.Length);
		
            Instance._playerSource.clip = Instance.walkStepClips[index];
            Instance._playerSource.Play();
        }

        public static void PlayJumpAudio()
        {
            Instance._playerSource.clip = Instance.jumpClip;
            Instance._playerSource.Play();
        }

        public static void PlayDeathAudio()
        {
            Instance._playerSource.clip = Instance.deathClip;
            Instance._playerSource.Play();
        }

        public static void PlaySceneRestartAudio()
        {
            Instance._musicSource.Stop();
            
            Instance._ambientSource.clip = Instance.ambientClip;
            Instance._ambientSource.loop = true;
            Instance._ambientSource.Play();
            
            Instance._stingSource.clip = Instance.levelStingClip;
            Instance._stingSource.Play();
        }

        public static void PlaySpawnAudio()
        {
            Instance._playerSource.clip = Instance.spawnClip;
            Instance._playerSource.Play();
        }
    }
}