using Core.Game;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Reboot.Game
{
    public class UiManager : UiManager<UiManager>
    {
        public string menuName = "MainMenu";
        public TextMeshProUGUI timeText;
        public TextMeshProUGUI countText;
        public GameObject menu;
        public GameObject lifePanel;
        public GameObject lifePrefab;
        public GameObject deathPrefab;
        public Image[] stars = new Image[3];

        public static void OnMenu()
        {
            Instance.menu.SetActive(true);
        }
        
        public static void OnHideMenu()
        {
            Instance.menu.SetActive(false);
        }

        public void OnPlay()
        {
            LevelManager.UnPause();
        }
        
        public void OnRestart()
        {
            LevelManager.UnPause();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public static void SetStars(int one, int two, int three)
        {
            var pos1 = Instance.stars[0].rectTransform.anchoredPosition;
            Instance.stars[0].rectTransform.anchoredPosition = new Vector2(pos1.x + one * 100f, pos1.y);
            
            var pos2 = Instance.stars[1].rectTransform.anchoredPosition;
            Instance.stars[1].rectTransform.anchoredPosition = new Vector2(pos2.x + two * 100f, pos2.y);
            
            var pos3 = Instance.stars[2].rectTransform.anchoredPosition;
            Instance.stars[2].rectTransform.anchoredPosition = new Vector2(pos3.x + two * 100f, pos3.y);
        }

        public static void UpdateReboot(int count, int max)
        {
            foreach (Transform child in Instance.lifePanel.transform) {
                Destroy(child.gameObject);
            }
            
            for (var i = 0; i < max; i++)
            {
                var g = Instantiate(i < count ? Instance.lifePrefab : Instance.deathPrefab, Instance.lifePanel.transform);
                var t = (RectTransform) g.transform;
                var anchoredPosition = t.anchoredPosition;
                t.anchoredPosition = new Vector2(anchoredPosition.x + i * 100f, anchoredPosition.y);
            }
        }

        public void OnExit()
        {
            SceneManager.LoadScene(Instance.menuName);
        }
        
        public static void UpdateTimeUi(float time)
        {
            var seconds = time % 60f;
            Instance.timeText.text = seconds.ToString("#0");
        }
        
        public static void UpdateCountUi(int rebootCount)
        {
            Instance.countText.text = rebootCount.ToString();
        }
    }
}