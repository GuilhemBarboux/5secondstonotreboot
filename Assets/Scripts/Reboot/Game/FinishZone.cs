﻿// This script is responsible for detecting collision with the player and letting the 
// Game Manager know

using UnityEngine;

namespace Reboot.Game
{
    [RequireComponent(typeof(Collider2D))]
    public class FinishZone : MonoBehaviour
    {
        private int _mPlayerLayer;
        
        private void Start()
        {
            _mPlayerLayer = LayerMask.NameToLayer($"Player");
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer != _mPlayerLayer) return;
            LevelManager.NextScene();
        }
    }
}