﻿using System;
using Reboot.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Reboot.Menu
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject menu;
        public GameObject credits;
        public GameObject levels;
        
        private void Start()
        {
            AudioManager.StartMenuAudio();
        }

        public void OnStart()
        {
            SceneManager.LoadScene("Level1");
        }

        public void OnExit()
        {
            Application.Quit();
        }
        
        public void OnSelectLevel(string id)
        {
            SceneManager.LoadScene(id);
        }

        public void OnLevels()
        {
            menu.SetActive(false);
            credits.SetActive(false);
            levels.SetActive(true);
        }

        public void OnCredits()
        {
            menu.SetActive(false);
            credits.SetActive(true);
            levels.SetActive(false);
        }

        public void OnMenu()
        {
            menu.SetActive(true);
            credits.SetActive(false);
            levels.SetActive(false);
        }
    }
}
